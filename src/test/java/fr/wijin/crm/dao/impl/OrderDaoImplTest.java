package fr.wijin.crm.dao.impl;

import static org.junit.jupiter.api.Assertions.fail;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.wijin.crm.model.Order;
import jakarta.persistence.EntityManager;
import jakarta.persistence.Persistence;

@TestMethodOrder(OrderAnnotation.class)
class OrderDaoImplTest {

	Logger logger = LoggerFactory.getLogger(OrderDaoImplTest.class);

	public static final String LABEL = "Formation Java";
	public static final String LASTNAME = "JONES";

	private static EntityManager em;
	private static OrderDaoImpl orderDao;

	@BeforeAll
	static void setup() {
		Map<String, String> properties = new HashMap<>();
		properties.put("hibernate.show_sql", "true");
		properties.put("hibernate.format_sql", "true");
		// Utilisation du "persistence unit" de test (BDD H2)
		em = Persistence.createEntityManagerFactory("crm-test", properties).createEntityManager();
		orderDao = new OrderDaoImpl(em);
	}

	@Test
	@org.junit.jupiter.api.Order(1)
	void givenOrderId_whenCallingTypedQueryMethod_thenReturnOrder() {
		Order order = orderDao.getById(1);
		logger.debug("Commande = {}", order);
		Assertions.assertNotNull(order, "Order not found");
	}

	@Test
	@org.junit.jupiter.api.Order(2)
	void givenOrderId_whenCallingTypedQueryMethod_thenReturnExpectedOrder() {
		Order order = orderDao.getById(1);
		Assertions.assertEquals(LABEL, order.getLabel(), "Order label should be " + LABEL);
	}

	@Test
	@org.junit.jupiter.api.Order(3)
	void givenOrderId_whenCallingTypedQueryMethod_thenReturnExpectedCustomer() {
		Order order = orderDao.getById(1);
		Assertions.assertEquals(LASTNAME, order.getCustomer().getLastname(), "Customer lastname should be " + LASTNAME);
	}

	@Test
	@org.junit.jupiter.api.Order(4)
	void whenCallingTypedQueryMethod_thenReturnListOfOrders() {
		List<Order> orders = orderDao.getAllOrders();
		Assertions.assertEquals(3, orders.size(), "Wrong number of orders");
	}

	@Test
	@org.junit.jupiter.api.Order(5)
	void whenCallingNamedQueryMethod_thenReturnListOfOrders() {
		List<Order> orders = orderDao.getOrdersByTypeAndStatus("Forfait", "En attente");
		Assertions.assertEquals(1, orders.size(), "Wrong number of orders for type and status");
	}

	@Test
	@org.junit.jupiter.api.Order(6)
	void whenCallingNativeQueryMethod_thenReturnListOfOrders() {
		List<Order> orders = orderDao.getOrdersForNumberOfDays(Double.valueOf(2.0));
		Assertions.assertEquals(2, orders.size(), "Wrong number of orders for a number of days greater than 2");
	}

	@Test
	@org.junit.jupiter.api.Order(7)
	void givenOrder_whenCallingTypedQueryMethodForCreate_thenReturnOK() {
		Order newOrder = new Order();
		newOrder.setLabel(LABEL);
		newOrder.setNumberOfDays(Double.valueOf(5));
		newOrder.setAdrEt(Double.valueOf(350));
		newOrder.setTva(Double.valueOf(20.0));
		newOrder.setType("Super commande");
		newOrder.setStatus("En cours");
		newOrder.setNotes("Les notes sur la commande");

		List<Order> orders = orderDao.getAllOrders();
		int numberOfOrdersBeforeCreation = orders.size();

		try {
			em.getTransaction().begin();
			orderDao.createOrder(newOrder);
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
			fail();
		}

		List<Order> ordersAfterCreation = orderDao.getAllOrders();
		int numberOfOrdersAfterCreation = ordersAfterCreation.size();
		Assertions.assertEquals(numberOfOrdersBeforeCreation + 1, numberOfOrdersAfterCreation);
	}

	@Test
	@org.junit.jupiter.api.Order(8)
	void givenOrder_whenCallingTypedQueryMethodForUpdate_thenReturnOK() {

		Order order = orderDao.getById(2);
		order.setLabel("Nouveau libellé");

		try {
			em.getTransaction().begin();
			orderDao.updateOrder(order);
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
			fail();
		}

		Order updatedOrder = orderDao.getById(2);
		Assertions.assertEquals("Nouveau libellé", updatedOrder.getLabel());
	}

	@Test
	@org.junit.jupiter.api.Order(9)
	void givenOrder_whenCallingTypedQueryMethodForDelete_thenReturnOK() {
		Order order = orderDao.getById(2);

		try {
			em.getTransaction().begin();
			orderDao.deleteOrder(order);
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
			fail();
		}

		Order deletedOrder = orderDao.getById(2);
		logger.debug("Deleted Order = {}", deletedOrder);
		Assertions.assertNull(deletedOrder, "Deleted order must be null");
	}

	@Test
	@org.junit.jupiter.api.Order(10)
	void whenCallingCriteriaUpdateQueryMethodForUpdate_thenReturnOK() {

		try {
			em.getTransaction().begin();
			orderDao.updateOrderStatus("Terminé", "En cours", "Forfait");
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
			fail();
		}

		List<Order> updatedOrders = orderDao.getOrdersByTypeAndStatus("Forfait", "Terminé");
		Assertions.assertEquals(1, updatedOrders.size(), "Wrong number of orders with type Forfait and status Terminé");
	}

}
