package fr.wijin.crm.dao.impl;

import java.util.HashMap;
import java.util.Map;

import fr.wijin.crm.dao.CustomerDao;
import fr.wijin.crm.dao.OrderDao;
import fr.wijin.crm.model.Customer;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityTransaction;
import jakarta.persistence.Persistence;

public class TestJpa {

	public static void main(String[] args) {
		Map<String, String> properties = new HashMap<>();
		properties.put("hibernate.show_sql", "true");
		properties.put("hibernate.format_sql", "true");

		EntityManager em = Persistence.createEntityManagerFactory("crm", properties).createEntityManager();
		CustomerDao customerDao = new CustomerDaoImpl(em);
		OrderDao orderDao = new OrderDaoImpl(em);

		System.out.println(customerDao.getAllCustomers().size());

		System.out.println(orderDao.getAllOrders().size());

		Customer customer = new Customer();
		customer.setLastname("Moi");
		customer.setFirstname("Moi");
		customer.setCompany("Mon entreprise");
		customer.setMail("mon.mail@lesmails.com");
		customer.setPhone("0222222222");
		customer.setMobile("0666666666");
		customer.setNotes("Les notes");
		customer.setActive(true);

		EntityTransaction transaction = null;
		try {
			transaction = em.getTransaction();

			transaction.begin();
			customerDao.createCustomer(customer);
			transaction.commit();
			System.out.println(customer.getId());
		} catch (Exception e) {
			System.out.println(e.getMessage());
			if (transaction != null) {
				transaction.rollback();
			}
		}

	}

}
